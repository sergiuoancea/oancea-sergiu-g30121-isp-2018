package oancea.sergiu.lab3.e1;

public class Robot {
        static int x;

        Robot() {
            x = 1;
            System.out.println("The robot's initial position is: " + x);
        }

        void change(int k) {
            if (k >= 1) {
                x = x + k;
                System.out.println("Robot's position updated: " + x);
            } else {
                System.err.println("Robot's position hasn't been updated! Argument k must be >=1!");
            }
        }

        public String toString(){
            return "Robot's position: " + x;
        }


    }
