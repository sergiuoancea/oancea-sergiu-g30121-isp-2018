package oancea.sergiu.lab3.e2;

public class Circle {
    private double radius = 1.0;
    private String color = "Red";

    Circle(double k) {  //this constructor decreases the radius by k if the result is greater than 0
        if (radius - k >= 0) {
            radius = radius - k;
            System.out.println("The new radius is: " + radius);
        } else {
            System.err.println("The radius can't be negative");
        }
    }

    Circle(int choose, double k, String changeColor) {  //this constructor modifies the attributes depending on the first variable 'choose'
        if (choose == 0) {
            radius = k;
            color = changeColor;
        } else if (choose == 1) {
            radius = radius + k;
            color = changeColor;
        }
        System.out.println("The radius is " + radius + " and the color is " + color);
    }

    void getRadius() {
        System.out.println("The current radius is: " + radius);
    }

    void getArea() {
        System.out.println("The area for the current radius is: " + (radius * radius) * 3.14);
    }


}
