package oancea.sergiu.lab3.e2;

public class TestCircle {
    public static void main(String[] args) {
        Circle c1 = new Circle(1.5);
        Circle c2 = new Circle(1, 3, "Blue");
        c1.getRadius();
        c2.getArea();
    }
}
