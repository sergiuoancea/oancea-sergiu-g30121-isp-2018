package oancea.sergiu.lab3.e3;

public class Author {
    private String name, email;
    private char gender;

    public Author(String name, String email, char gender1) {
        this.name = name;
        this.email = email;
        if ((gender1 == 'm') || (gender1 == 'f')) {
            this.gender = gender1;
        } else {
            System.err.println("Please enter a valid gender(m or f)!");
        }
        System.out.println("Author's name is: " + name);
        System.out.println("Author's email is: " + email);
        System.out.println("Author's gender is: " + gender + "\n\n");
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public char getGender() {
        return gender;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String toString() {
        return "Author - " + name + " (" + gender + ") at " + email;
    }

}
