package oancea.sergiu.lab3.e3;

public class TestAuthor {
    public static void main(String[] args) {
        Author a1 = new Author("Popescu Popovici", "popescu.popovici@email.com", 'a');
        Author a2 = new Author("Popescu Popoviciu", "popescu.popoviciu@email.com", 'm');

        String a = a2.getName();
        System.out.println("getName: " + a);

        String b = a2.getEmail();
        System.out.println("getEmail: " + b);

        char c = a2.getGender();
        System.out.println("getGender: " + c);

        a2.setEmail("popescu.popoviciu1@yahoo.com");

        String d = a2.getEmail();
        System.out.println("getEmail after using setEmail: " + d + "\n");

        System.out.println(a2);
    }
}
