package oancea.sergiu.lab3.e4;

public class TestMyPoint {

    public static void main(String[] args) {

        MyPoint p1 = new MyPoint();
        int a = p1.getX();
        System.out.println("getX: " + a);
        int b = p1.getY();
        System.out.println("getY: " + b);

        MyPoint p2 = new MyPoint(2, 2);
        int c = p2.getX();
        System.out.println("getX: " + c);
        int d = p2.getY();
        System.out.println("getY: " + d);

        p2.setX(3);
        p2.setY(3);
        c = p2.getX();
        System.out.println("getX after using setX: " + c);
        d = p2.getY();
        System.out.println("getY after using setY: " + d);

        p2.setXY(4, 4);
        c = p2.getX();
        System.out.println("getX after using setXY: " + c);
        d = p2.getY();
        System.out.println("getY after using setXY: " + d);

        System.out.println(p2);

        double distance1 = p1.distance(2, 2);
        System.out.println("The distance between this object and given coordinates is: " + distance1);


        MyPoint p3 = new MyPoint(2, 2);
        p2.setXY(4, 5);

        double distance2 = p2.distance(p3);
        System.out.println("The distance between this object and given object is: " + distance2);

    }
}
