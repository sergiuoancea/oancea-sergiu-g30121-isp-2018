package oancea.sergiu.lab3.e5;

public class Flower {
    int petal;
    static int count = 0;


    Flower(int p) {
        petal = p;
        System.out.println("New flower has been created!");
        count++;
    }

    static void NumberOfObjects() {
        System.out.println("The number of objects that were created is: " + count);
    }

    public static void main(String[] args) {
        Flower f1 = new Flower(4);
        Flower f2 = new Flower(6);
        Flower f3 = new Flower(3);
        NumberOfObjects();

    }

}
