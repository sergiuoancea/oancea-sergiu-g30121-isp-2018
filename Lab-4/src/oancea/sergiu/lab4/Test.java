package oancea.sergiu.lab4;

import oancea.sergiu.lab4.e2.Author;
import oancea.sergiu.lab4.e3.Book;
import oancea.sergiu.lab4.e4.Book1;
import oancea.sergiu.lab4.e5.Cylinder;
import oancea.sergiu.lab4.e6.Circle;
import oancea.sergiu.lab4.e6.Square;

public class Test {
    public static void main(String[] args) {
        //testing Author-Book
        Author a1=new Author("Popescu Popovici","popescu.popovici@yahoo.com",'m');
        Author a2=new Author("Patricia Popovici","patricia.popovici@yahoo.com",'f');
        System.out.println(a1);
        Book b1=new Book("BlaBla BlaBla",a1,120,5);
        System.out.println(b1);

        Author[] authors=new Author[2];
        authors[0]=a1;
        authors[1]=a2;

        Book1 b2=new Book1("BlaBla BlaBla vol. 2",authors,100,3);
        System.out.println(b2);

        b2.printAuthors();

        //----------------------------------------------------------------------------------------------------

        //testing Circle-Cylinder

        Cylinder c1=new Cylinder(2.0,3.0);

        double c1Area=c1.getArea();
        System.out.println("Area is "+c1Area);

        double c1Volume=c1.getVolume();
        System.out.println("Volume is "+c1Volume);

        //----------------------------------------------------------------------------------------------------

        //testing Shape-Circle

        Circle c4 = new Circle("blue", false, 2.0);
        System.out.println(c4);
        double c4Area=c4.getArea();
        System.out.println("Area is "+c4Area);
        double c4Perimeter=c4.getPerimeter();
        System.out.println("Area is "+c4Perimeter);


        //testing Square-Rectangle-Shape

        Square s1=new Square(3);
        double s=s1.getSide();
        System.out.println(s);
        s1.setSide(4);
        double s2=s1.getSide();
        System.out.println(s2);

    }
}
