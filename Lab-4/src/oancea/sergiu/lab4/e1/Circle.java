package oancea.sergiu.lab4.e1;

public class Circle {
    private double radius = 1.0;
    private String color = "red";

    public Circle() {
        System.out.println("This constructor does nothing!");
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public double getArea() {
        return 3.14 * this.radius * this.radius;
    }

    public String toString() {
        return "The circle is " + color + " with the radius of " + radius;
    }

}
