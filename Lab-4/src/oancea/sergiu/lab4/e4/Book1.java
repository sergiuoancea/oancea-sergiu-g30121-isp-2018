package oancea.sergiu.lab4.e4;

import oancea.sergiu.lab4.e2.Author;

public class Book1 {
    private String name;
    private Author[] authors;
    private double price;
    private int qtyInStock;


    public Book1(String name, Author[] authors, double price) {
        this.name = name;
        this.authors = authors;
        this.price = price;
    }

    public Book1(String name, Author[] authors, double price, int qtyInStock) {
        this.name = name;
        this.authors = authors;
        this.price = price;
        this.qtyInStock = qtyInStock;
    }

    public String getName() {
        return name;
    }

    public Author[] getAuthors() {
        return authors;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQtyInStock() {
        return qtyInStock;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }

    public String toString() {
        return "'" + name + "' by " + this.authors.length + " authors.";
    }

    public void printAuthors() {
        int i;
        for (i = 0; i < this.authors.length; i++) {
            System.out.println("Author name: " + this.authors[i].getName());
        }
    }

}

