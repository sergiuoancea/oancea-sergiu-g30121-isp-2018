package oancea.sergiu.lab5.e1;


public class Test {
    public static void main(String[] args) {
        //testare Circle
        Circle c4 = new Circle("blue", false, 3.6);


        //testare Rectangle

        Rectangle r1 = new Rectangle("blue", false, 3.6, 4.0);


        //testare Square

        Square s1 = new Square("blue", false, 4.0);


        Shape Array[] = {c4, r1, s1};
        for (int i = 0; i < Array.length; i++) {
            double Area = Array[i].getArea();
            System.out.println("Area is " + Area);
            double Perimeter = Array[i].getPerimeter();
            System.out.println("Perimeter is " + Perimeter);
            System.out.println(Array[i]);
        }


    }
}
