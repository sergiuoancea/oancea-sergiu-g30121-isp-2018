package oancea.sergiu.lab5.e2;

public class Test {
    public static void main(String[] args) {
        RealImage reI=new RealImage("Real Image");
        reI.display();
        RotatedImage roI=new RotatedImage("Rotated Image");
        roI.display();
        ProxyImage prIre=new ProxyImage("Real Proxy Image","Real");
        prIre.display();
        ProxyImage prIro=new ProxyImage("Rotated Proxy Image","Rotated");
        prIro.display();

    }
}
