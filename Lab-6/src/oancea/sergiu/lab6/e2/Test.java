package oancea.sergiu.lab6.e2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Test {

    public static void main(String[] args) {

        Bank bank = new Bank();
        bank.addAccount("Mihai",240);
        bank.addAccount("George",370);
        bank.addAccount("Andrei",140);
        bank.addAccount("Alex",9700);

        bank.printAccounts();
        System.out.println("------------------------------");
        bank.printAccounts(200,300);
        System.out.println("------------------------------");
        System.out.println(bank.getAccount("Mihai").getBalance());

        System.out.println("------------------------------");



    }
}
