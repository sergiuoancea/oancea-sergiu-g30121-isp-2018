package oancea.sergiu.lab8.e4;

public class AlarmUnit extends Unit {


    public AlarmUnit() {
        super(UnitType.ALARM);
    }

    @Override
    public String toString() {
        return "FIRE Alarm ON";
    }
}
