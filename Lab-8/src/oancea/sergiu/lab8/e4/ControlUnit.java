package oancea.sergiu.lab8.e4;

public class ControlUnit {

    private static ControlUnit unit;

    private ControlUnit() {
    }

    public static ControlUnit getInstance() {

        if (unit == null) {
            unit = new ControlUnit();
        }
        return unit;
    }
}
