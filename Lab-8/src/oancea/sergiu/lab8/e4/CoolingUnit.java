package oancea.sergiu.lab8.e4;

public class CoolingUnit extends Unit {
    public CoolingUnit() {
        super(UnitType.COOLING);
    }

    @Override
    public String toString() {
        return "Cooling unit ON";
    }
}
