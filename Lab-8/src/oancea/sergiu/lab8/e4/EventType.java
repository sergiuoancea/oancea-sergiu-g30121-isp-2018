package oancea.sergiu.lab8.e4;

public enum EventType {
    TEMPERATURE, FIRE, NONE;
}
