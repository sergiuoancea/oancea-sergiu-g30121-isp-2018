package oancea.sergiu.lab8.e4;

public class FireSensor extends FireEvent {

    FireSensor(boolean smoke) {
        super(smoke);
    }

    @Override
    public String toString() {
        if (isSmoke()) {
            return "\n" + new AlarmUnit() + new GsmUnit();
        } else {
            return "\n" + "No Action,everything is alright";
        }
    }
}
