package oancea.sergiu.lab8.e4;

public class HeatingUnit extends Unit{


    public HeatingUnit() {
        super(UnitType.ALARM.HEATING);
    }

    @Override
    public String toString() {
        return "Heating unit ON";
    }
}
