package oancea.sergiu.lab8.e4;

class TemperatureEvent extends Event {

    private int vlaue;
    private static final int preset = 23;

    TemperatureEvent(int vlaue) {
        super(EventType.FIRE.TEMPERATURE);
        this.vlaue = vlaue;
    }

    int getVlaue() {
        return vlaue;
    }

    @Override
    public String toString() {
            return "TemperatureEvent{" + "vlaue=" + vlaue + '}';//+ new TemperatureSensor(vlaue).toString();
    }


}
