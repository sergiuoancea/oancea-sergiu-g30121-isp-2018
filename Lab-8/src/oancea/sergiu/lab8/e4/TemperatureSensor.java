package oancea.sergiu.lab8.e4;

public class TemperatureSensor extends TemperatureEvent {

    protected static final int preset = 23;

    TemperatureSensor(int vlaue) {
        super(vlaue);
    }

    @Override
    public String toString() {
        if (getVlaue() < preset) {
            return "\n" + "Temp is lower" + "\n" + new HeatingUnit();
        } else if (getVlaue() > preset) {
            return "\n" + "Temp is higher" + "\n" + new CoolingUnit();
        } else {
            return "\n" + "The temperature is perfect!";
        }
    }

}
