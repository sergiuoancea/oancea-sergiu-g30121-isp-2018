package oancea.sergiu.lab8.e4;

abstract class Unit {
    UnitType type;

    Unit(UnitType type){
        this.type = type;
    }

    UnitType getType(){
        return type;
    }

}
