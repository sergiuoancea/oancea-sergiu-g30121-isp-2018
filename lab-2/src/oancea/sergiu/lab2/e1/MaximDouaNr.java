package oancea.sergiu.lab2.e1;

import java.util.Scanner;

public class MaximDouaNr {
    public static void main(String[] args){
        Scanner scan=new Scanner(System.in);
        System.out.println("Dati 2 numere:");
        int x = scan.nextInt();
        int y = scan.nextInt();
        if(x>y){
            System.out.println("Maximul este "+x);
        }
        else if(y>x){
            System.out.println("Maximul este "+y);
        }
    }
}
