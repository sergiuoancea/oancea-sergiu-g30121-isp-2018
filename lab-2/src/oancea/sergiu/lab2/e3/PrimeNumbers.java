package oancea.sergiu.lab2.e3;

import java.util.Scanner;

public class PrimeNumbers {
    public static void main(String args[]) {
        int A, B, prim = 0, i, j;
        Scanner in = new Scanner(System.in);
        System.out.println("Please enter the first number A:");
        A = in.nextInt();
        System.out.println("Please enter the second number B:");
        B = in.nextInt();
        System.out.println("The prime numbers are:");
        for (i = A+1; i < B; i++) {
            for (j = 2; j < i; j++) {
                if (i % j == 0) {
                    prim = 0;
                    break;
                } else
                    prim = 1;
            }
            if (prim == 1) {
                System.out.println(i);
            }
        }
    }
}
