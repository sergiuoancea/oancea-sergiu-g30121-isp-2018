package oancea.sergiu.lab2.e4;

public class MaxVector {
    public static void main(String args[]){
        int [ ]N = {142,3,347,82,900};
        int i,max=N[0];
        for (i=1;i<N.length;i++){
            if (N[i]>max){
                max=N[i];
            }
        }
        System.out.println("The maximum element of the vector is:"+max);

    }
}
