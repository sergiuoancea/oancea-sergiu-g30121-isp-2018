package oancea.sergiu.lab2.e6;

import java.util.Scanner;

public class Factorial {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter a number:");
        int x = in.nextInt();
        int i = 1, p = 1;
        for (i = 1; i <= x; i++) {
            p = p * i;
        }
        System.out.println("The factorial value of " + x + " is: " + p);
    }
}
